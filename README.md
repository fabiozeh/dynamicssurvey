# Dynamics Expressivity Perception Survey

Code for a Web application built for running a perceptual evaluation of computer-generated variations in violin performance dynamics.

Deployed in PythonAnywhere.

### Deployment details

1. Clone the repository from the bash console in Python Anywhere.
2. Set the directories for code and static content.
3. Create the database with name, user, and password as specified in the flask app connection.
4. Run the flask and, in the console, run `db.create_all()`
5. Done

